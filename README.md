# Graboid

## Team Name
[DirtDragons](https://tremors.fandom.com/wiki/Dirt_Dragon)

## Members
| Name          | Major            | email               |
| ------------- | ---------------- | ------------------- |
| Aaron Boyd    | Computer Science | boyda7@mail.uc.edu  |
| Brandon Do    | Computer Science | dobt@mail.uc.edu    |
| Brandon Odell | Computer Science | odellba@mail.uc.edu |
| Robert Deal   | Computer Science | dealre@mail.uc.edu  |

## Project Topic
Computer worm research and development.
